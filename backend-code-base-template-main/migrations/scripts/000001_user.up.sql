CREATE TABLE IF NOT EXISTS users(
    id SERIAL PRIMARY KEY,
    email varchar(50) NOT NULL UNIQUE,
    password varchar(250) NOT NULL,
    roleId int
);
