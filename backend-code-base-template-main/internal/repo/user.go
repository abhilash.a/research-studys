package repo

import (
	"backend-code-base-template/internal/entities"
	"database/sql"
	"fmt"
)

type UserRepo struct {
	db *sql.DB
}

type UserRepoImply interface {
	GetUsers() []entities.User
	InsertUser(payload entities.Payload) int64
	DeleteUser(payload entities.Payload) int64
}

// NewUserRepo
func NewUserRepo(db *sql.DB) UserRepoImply {
	return &UserRepo{db: db}
}

// GetUsers
func (user *UserRepo) GetUsers() []entities.User {
	val, err := user.db.Query(`select * from users`)
	if err != nil {
		fmt.Println("get db error", err)
	}
	var details []entities.User
	var detail entities.User
	for val.Next() {
		err = val.Scan(&detail.ID, &detail.Email, &detail.Password, &detail.RoleID)
		if err != nil {
			fmt.Println("scanfails", err)
		}
		details = append(details, detail)
	}

	return details
}

func (ins *UserRepo) InsertUser(payload entities.Payload) int64 {
	row := ins.db.QueryRow(`insert into users(id,email,password,roleid) values($1,$2,$3,$4) returning id`, payload.ID, payload.Email, payload.Password, payload.RoleID)
	var id entities.User
	err := row.Scan(&id.ID)
	if err != nil {
		fmt.Println("id return error", err)
	}
	return int64(id.ID)
}

func (ins *UserRepo) DeleteUser(payload entities.Payload) int64 {
	row := ins.db.QueryRow(`delete from users where id=$1 returning id`, payload.ID)
	var id entities.User
	err := row.Scan(&id.ID)
	if err != nil {
		fmt.Println("id return error", err)
	}
	return int64(id.ID)
}
