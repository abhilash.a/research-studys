package entities

type User struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	RoleID   int    `json:"roleId"`
}
type Payload struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	RoleID   int    `json:"roleId"`
}
