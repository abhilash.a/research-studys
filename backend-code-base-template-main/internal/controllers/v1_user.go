package controllers

import (
	"backend-code-base-template/internal/entities"
	"net/http"

	"github.com/gin-gonic/gin"
)

// V1_HealthHandler
// this will work when the api version provide as v1
func (user *UserController) V1_HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with v1 version",
	})
}

func (user *UserController) V1_GetUsers(ctx *gin.Context) {
	result := user.useCases.GetUsers()
	ctx.IndentedJSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": result,
	})
}
func (user *UserController) V1_InsertUser(ctx *gin.Context) {
	var payload entities.Payload
	ctx.BindJSON(&payload)
	result := user.useCases.InsertUser(payload)
	ctx.IndentedJSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": result,
		"account": "created",
	})
}
func (user *UserController) V1_DeleteUser(ctx *gin.Context) {
	var payload entities.Payload
	ctx.BindJSON(&payload)
	result := user.useCases.DeleteUser(payload)
	ctx.IndentedJSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": result,
		"account": "deleted",
	})
}
