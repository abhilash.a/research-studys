package controllers

import (
	"backend-code-base-template/internal/usecases"
	"backend-code-base-template/version"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserController struct {
	router   *gin.RouterGroup
	useCases usecases.UserUseCaseImply
}

// NewUserController
func NewUserController(router *gin.RouterGroup, userUseCase usecases.UserUseCaseImply) *UserController {
	return &UserController{
		router:   router,
		useCases: userUseCase,
	}
}

// InitRoutes
func (user *UserController) InitRoutes() {
	// user.router.Handle(http.MethodGet, "/health", func(ctx *gin.Context) {
	// 	version.RenderHandler(ctx, user, "HealthHandler")
	// })

	user.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, user, "HealthHandler")
	})
	user.router.GET("/:version/user", func(ctx *gin.Context) {
		version.RenderHandler(ctx, user, "GetUsers")
	})
	user.router.POST("/:version/user", func(ctx *gin.Context) {
		version.RenderHandler(ctx, user, "InsertUser")
	})
	user.router.DELETE("/:version/user", func(ctx *gin.Context) {
		version.RenderHandler(ctx, user, "DeleteUser")
	})
}

// HealthHandler
func (user *UserController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}
