package usecases

import (
	"backend-code-base-template/internal/entities"
	"backend-code-base-template/internal/repo"
)

type UserUseCases struct {
	repo repo.UserRepoImply
}

type UserUseCaseImply interface {
	GetUsers() []entities.User
	InsertUser(payload entities.Payload) int64
	DeleteUser(payload entities.Payload) int64
}

// NewUserUseCases
func NewUserUseCases(userRepo repo.UserRepoImply) UserUseCaseImply {
	return &UserUseCases{
		repo: userRepo,
	}
}

// GetUsers
func (user *UserUseCases) GetUsers() []entities.User {
	return user.repo.GetUsers()
}

func (user *UserUseCases) InsertUser(payload entities.Payload) int64 {
	return user.repo.InsertUser(payload)
}

func (user *UserUseCases) DeleteUser(payload entities.Payload) int64 {
	return user.repo.DeleteUser(payload)
}
