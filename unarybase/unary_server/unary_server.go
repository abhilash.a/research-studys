package main

import (
	"context"
	"log"
	"math/rand"
	"net"

	pb "gitlab.com/abhilash.a/research-studys/unarybase/unary"
	"google.golang.org/grpc"
)

const (
	port string = ":5000"
)

type UserManagementServer struct {
	pb.UnimplementedUserManagementServer
	user_list *pb.UserList
}

func (s *UserManagementServer) CreateNewUser(ctx context.Context, in *pb.NewUser) (*pb.User, error) {
	log.Printf("recieved: %v", in.GetName())
	var user_id int32 = int32(rand.Intn(1000))
	createdUser := &pb.User{
		Name: in.GetName(),
		Age:  in.GetAge(),
		Id:   user_id,
	}
	s.user_list.Users = append(s.user_list.Users, createdUser)
	return createdUser, nil
}

func NewUserManagementServer() *UserManagementServer { //constructor to intialise userManagement server to add new user inlist
	return &UserManagementServer{
		user_list: &pb.UserList{},
	}
}
func (server *UserManagementServer) Run() error {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Printf("listening error: %v", err)
	}
	//invoking grpcfunction from grpc module
	s := grpc.NewServer()
	//registering server as new grpc service
	pb.RegisterUserManagementServer(s, server)
	log.Printf("server listening @ port:%v", lis.Addr())
	return s.Serve(lis)

}
func (s *UserManagementServer) GetUsers(ctx context.Context, in *pb.GetUsersParams) (*pb.UserList, error) {
	return s.user_list, nil
}
func main() {
	var user_mgmt_server *UserManagementServer = NewUserManagementServer()
	if err := user_mgmt_server.Run(); err != nil {
		log.Fatalf("server run error:%v", err)
	}
}
