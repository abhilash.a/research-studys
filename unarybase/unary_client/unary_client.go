package main

import (
	"context"
	"fmt"
	"log"
	"time"

	pb "gitlab.com/abhilash.a/research-studys/unarybase/unary"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	address = "localhost:5000"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		log.Fatalf("connect to localhost error:%v", err)
	}
	defer conn.Close()
	c := pb.NewUserManagementClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var new_users = make(map[string]int32)
	new_users["Alice"] = 43
	new_users["Anhi"] = 27
	for name, age := range new_users {
		r, err := c.CreateNewUser(ctx, &pb.NewUser{
			Name: name,
			Age:  age,
		})
		if err != nil {
			log.Fatalf("create user input error:%v", err)
		}
		log.Printf(`User details from server:
		Name:%s,
		Age:%v,
		Id:%v`, r.GetName(), r.GetAge(), r.GetId())
	}
	params := &pb.GetUsersParams{}
	r, err := c.GetUsers(ctx, params)
	if err != nil {
		log.Fatalf("couldnot retrieve :%v", err)
	}
	log.Println("UserLIst slice")
	fmt.Printf("r.Getuser():%v\n", r.GetUsers())

}
