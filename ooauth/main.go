package main

import (
	"log"
	"net/http"
	"ooauth/controllers"
)

func main() {
	fs := http.FileServer(http.Dir("public"))
	http.Handle("/", fs)
	http.HandleFunc("/signin", controllers.Signin)
	http.HandleFunc("/callback", controllers.Callback)
	http.HandleFunc("/hello", controllers.Hello)
	log.Fatal(http.ListenAndServe(":3000", nil))

}
