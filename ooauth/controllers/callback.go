package controllers

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
)

func Callback(w http.ResponseWriter, r *http.Request) { //from the redirecturl function resieves all parameters with authcode and state
	State := r.FormValue("state")
	Code := r.FormValue("code") //authcode from request from redirecturl function
	data, err := getUserData(State, Code)
	if err != nil {
		fmt.Println("error callback is ", err)
	}
	n, err := fmt.Fprintf(w, "Data:%s", string(data))
	if err != nil {
		fmt.Println("err", err, n)
	}

}
func getUserData(state, code string) ([]byte, error) {
	if state != RandomString {
		return nil, errors.New("invalid user")
	}
	token, err := ssogolang.Exchange(context.Background(), code) //post request authorisation code
	if err != nil {
		fmt.Println("error in userdata func", err)

	}
	response, err := http.Get("https://www.googleapis.com/oauth/2/v2/userinfo?accesstoken=" + token.AccessToken)
	if err != nil {
		fmt.Println("error in token respone", err)
		return nil, err
	}
	defer response.Body.Close()
	data, err := io.ReadAll(response.Body)
	fmt.Println("response", data)
	return data, err
}
