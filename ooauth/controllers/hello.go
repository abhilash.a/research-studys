package controllers

import (
	"fmt"
	"net/http"
)

func Hello(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	_, err := fmt.Fprintf(w, "hello")
	if err != nil {
		fmt.Println("error is ", err)
	}
	w.Write([]byte(`hello`))
}
